import express, { Request, Response } from "express";
import moviesService from "./movies.service";

const router = express.Router();

router.use(express.json());

router.get("/id/:id", async (req: Request, res: Response) => {

    const id = req?.params?.id;
    try {
        let movie = await moviesService.getById(id);
        if (!movie)
            res.status(404).send(`No se encontró el película con id: ${id}`);
        else
            res.json(movie);
    } catch (error) {
        res.status(404).send(`No se encontró el película con id: ${id}`);
    }
});

router.get("/find", async (req: Request, res: Response) => {
    
    let countries = typeof req?.query?.countries === "string" ? req.query.countries.split(",") : [];
    let awards = typeof req?.query?.awards === "string" ? req.query.awards.split(",") : [];

    if (!(countries instanceof Array) || !(awards instanceof Array))
        res.status(400).send();

    try {

        let movies = await moviesService.filter(countries, awards)

        res.json(movies);
    }
    catch (error) {
        res.status(404).send(`No se encontraron pelíulas`);
    }
});
//Filtro por rango de años
router.get("/findYear", async (req: Request, res: Response) => {

    let year = typeof req?.query?.year === "string" ? req.query.year.split(",") : [];
    
    if (!(year instanceof Array))
        res.status(400).send();

    try {

        let movies = await moviesService.filterYear(year)

        res.json(movies);
    }
    catch (error) {
        res.status(404).send(`No se encontraron pelíulas`);
    }
});
export default router;