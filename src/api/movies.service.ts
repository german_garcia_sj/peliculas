import { ObjectId } from "bson"
import { Collection, Db, MongoClient } from "mongodb"
import { convertCompilerOptionsFromJson } from "typescript";
import { Movie } from "../models/movie";

var db: Db;
var collections: { movies?: Collection<Movie> } = {};

export default class MoviesService {

    static async injectDB(client: MongoClient) {
        if (db) {
            return;
        }
        try {
            await client.connect();

            db = client.db(process.env.MFLIX_DB_NAME);

            collections.movies = db.collection(process.env.MOVIES_COLLECTION_NAME || "");

        } catch (e) {
            console.error(e);
            throw `No se puedo establecer una conexión con la bd en MoviesService: ${e}`;
        }
    }

    static async getById(id: string) {
        if (!collections.movies)
            return null;

        try {
            let movie = collections.movies?.findOne({ _id: new ObjectId(id) });
            return movie;
        } catch (e) {
            throw "Algo va mal";
        }
    }
//Filtro por premios y pais
    static async filter(countries: Array<string>, awards: Array<string>) {
    console.log(awards)
        let cursor = await collections.movies?.find({ $or: [ 
                                                    {awards: { $eq: awards }},
                                                    {countries: { $in: countries }} 
                                                            ]}).limit(10);

        if ((await cursor?.count()) === 0)
            return [];

        let movies: Array<Movie> = [];

        await cursor?.forEach(c => {
            movies.push(c as Movie);
        });

        return movies;
    }
//Filtro por rango de años
    static async filterYear(year: Array<string>) {
        
        let year1 = Number(year[0])
        let year2 = Number(year[1])
        console.log(year1, year2)
        let cursor
        if (year1 < year2){
            /*let*/ cursor = await collections.movies?.find({ year:{$lte: year2,$gte: year1}}).limit(10);
        }
        else{
            /*let*/ cursor = await collections.movies?.find({ year:{$lte: year1,$gte: year2}}).limit(10);
        }
       

        if ((await cursor?.count()) === 0)
            return [];

        let movies: Array<Movie> = [];

        await cursor?.forEach(c => {
            movies.push(c as Movie);
        });

        return movies;
    }
}